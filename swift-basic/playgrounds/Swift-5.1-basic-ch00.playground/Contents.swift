// swift version 5.1
// Chapter 00 Basic application

// Declaring const and variable

let someConst = 1
var someVariable = "dwa"


 // someConst = 2 // it is not allowed

// Type adnotation

let someStringConst: String = "Some String value"


// String interpolation


let out1 = "someStringConst = \(someStringConst)"

// Number types

let int_v1:Int = 1
let uint_v2:UInt = 2
let float_v3:Float = 3
let double_v4:Double = 4
let aa=[1,2,3,4]


// control flow
let a=1

if a == 1 {
    print("a=\(a)")
}

// if and let and optional
let name:String? = "Dawid"

if let newName = name {
    print("new name = \(newName)")
}

let name2:String? = nil

if let newName = name2 {
    print("new name = \(newName)") // block is not analyzed
}

// Optional with ?? char (default)
let someNewConst = name2 ?? "Unknown"

// for in with list and tuble

let interestingNumber = [
    "prime": [2, 3, 57],
    "fibonaczi": [1, 1, 2, 3, 5, 8],
    "cos-tam": [1, 4, 9, 16, 25 ]
]

for (key, value) in interestingNumber {
    print("key: \(key) value: \(value)")
}

// for with ..<

for i in 1..<100 {
    i
}


// functions

func sum(a:Int,b:Int) -> Int {
    return a+b
}

sum(a:1,b:2)

func sum2(_ a:Int, _ b:Int) -> Int {
    return a + b
}

sum2(5,2)

func calculate(n1:Int, n2:Int, f:(Int,Int)->Int) -> Int {
    return f(n1,n2)
}

calculate(n1: 20, n2:30, f: {a,b in a*b})


// classes

class A  {
    let number:Int
    
    init(_ a:Int) {
        self.number = a;
    }
}

let aVal = A(2)
aVal.number

class B:A {
    func intAndReturn() -> Int {
        return self.number+1
    }
}


let bVal = B(10)
bVal.intAndReturn()

class C {
    private var number:Int
    
    init(_ n:Int) {
        self.number = n
    }
    
    func getRawNumber() -> Int{
        return self.number
    }
    
    var kb:Int {
        get {
            return self.number / 1024
        }
        set {
            self.number = newValue * 1024
        }
    }
}

let cVal = C(20)
cVal.kb = 2
cVal.kb
cVal.getRawNumber()


//Enums
enum Planet:Int {
    case merucy = 1
    case venus, earth, mars, jupiter, saturn, uranus, neptune

    func isReadyForHumanLiving() -> Bool {
        switch self {
        case .earth:
            return true
        default:
            return false
        }
    }
}

Planet.earth.isReadyForHumanLiving()
Planet.mars.isReadyForHumanLiving()








