// Swift 5.1 Structures and Classes
 
struct Resolution {
    var width = 0
    var height = 0
}

class VideoMode {
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name: String?
}
 
 
let res1 = Resolution()
 
let videoMode1 = VideoMode()
 
// Memberwise Initializers for Structure Types
 
let res800x600 = Resolution(width: 800, height: 600)
 
res800x600.height + res800x600.height
var resCloneof800x600 = res800x600 // It clreate clone of orgial variable
resCloneof800x600.height = 599
 
print("new value of for clone of is res800x600 \(resCloneof800x600)")
 
 
let tenEighty  = VideoMode()
let alsoTenEighty = tenEighty
 
if tenEighty === alsoTenEighty {
    print("tenEighty and alsoTenEighty refer to the same VideoMode instance.")
}
