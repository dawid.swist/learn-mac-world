// swift version 5.1
// Chapter 02 Basic operators

// assignment

let (a,b) = (1,2)

//Ternary operator

let c = a==1 ? "yes": "no"

var nill:Int? = nil

var optionalB:Int? = b
var optionalA:Int? = a

let d = optionalA ?? 1

let e = nill ?? 0

// Range Operators

for i in 1...5 {
    print ("Range Operators i=\(i)")
}


// Half-Open Range Operator

let names = ["Anna", "Alex", "Brian", "Jack"]
let count = names.count
for i in 0..<count {
    print("Person \(i + 1) is called \(names[i])")
}

// One-Sided Ranges

for name in names[2...] {
    print(name)
}
// Brian
// Jack

for name in names[...2] {
    print(name)
}
// Anna
// Alex
// Brian

for name in names[..<2] {
    print(name)
}
// Anna
// Alex

let range = ...5
range.contains(7)   // false
range.contains(4)   // true
range.contains(-1)  // true


