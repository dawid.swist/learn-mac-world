// swift version 5.1
// Chapter 03 Collections

// List create

var list_1 = [1,2,3,4] // mutable
let list_2 = [1,2,4,5] // immutable
let list_3 = list_1 + list_2
var list_4:[Int] = []

// list operate

list_1.append(5)
list_2[1]
list_1.isEmpty

for element in list_2 {
    element
}

for (i,v) in list_1.enumerated() {
    print ("i=\(i) v=\(v)")
}

list_1.remove(at: 3)

//Set

let set_1 = Set([1,2,3,4])
var set_2 = Set(list_1)

set_2.insert(7)
// set_1.insert(7) not allow

if let removedElement = set_2.remove(1) {
    print("Removed element is \(removedElement)")
}

//

let elem_1_5 = Set([1,2,3,4,5])
let elem_4_8 = Set([4,5,6,7,8])

elem_1_5.intersection(elem_4_8)
elem_4_8.symmetricDifference(elem_1_5)
elem_4_8.union(elem_1_5)
elem_1_5.subtracting(elem_4_8)

// Dictionary/map

let map_1: [String: Int] = ["one": 1, "two": 2]
let map_2: [String: Int] = ["a": 10, "b": 20]


if map_1.isEmpty {
    print("map is empty")
} else {
    print("map = \(map_1)")
}

var map_3 = map_1

map_3.updateValue(3, forKey: "three")

map_3["two"] = 22
print (map_3)


for key in map_3.keys {
    print (key)
}

for value in map_3.values {
    print(value)
}









