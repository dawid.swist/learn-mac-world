import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(iOS_12_introducingTests.allTests),
    ]
}
#endif
