//
//  Ch02Functional-define.swift
//  iOS-12-introducingTests
//
//  Created by Dawid Świst on 05/05/2020.
//

import XCTest

class Ch02FunctionsDefine: XCTestCase {
    func testExample() throws {
        // Define simple functions

        func add1(x: Int, y: Int) -> Int {
            return x + y
        }

        func add2(_ x: Int, _ y: Int) -> Int {
            return x + y
        }

        func add3(x: Int, y: Int = 0) -> Int { // default value of paramters
            x + y
        }

        func add4(_ x: Int ...) -> Int {
            var sum: Int = 0
            for i: Int in x { sum += i }

            return sum
        }

        func add(_ s1: String, _ s2: String) -> String { // functiion overloadng.
            return s1 + s2
        }

        func print2(x: String) {
            print(x)
        }

        XCTAssertEqual(5, add1(x: 3, y: 2))
        XCTAssertEqual(3, add2(1, 2))
        XCTAssertEqual(1, add3(x: 1))
        XCTAssertEqual(10, add4(1, 2, 3, 4))
        XCTAssertEqual("Hello-World!", add("Hello", "-World!"))
    }
}
