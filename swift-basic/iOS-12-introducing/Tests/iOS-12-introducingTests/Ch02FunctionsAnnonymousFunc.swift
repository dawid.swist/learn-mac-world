//
//  Ch02FunctionsAnnonymousFunc.swift
//  iOS-12-introducingTests
//
//  Created by Dawid Świst on 05/05/2020.
//

import XCTest

class Ch02FunctionsAnnonymousFunc: XCTestCase {
    func testFunctionDefinition() throws {
        func op1(_ x: Int, _ y: Int) -> Bool { // Some function definition
            return x > y
        }

        func executeOp(x: Int, y: Int, op: (Int, Int) -> Bool) -> Bool {
            return op(x, y)
        }

        func exeuteOpWithoutParameter(_ op: () -> String) -> String {
            return op()
        }

        // use reference to function op1
        XCTAssertTrue(executeOp(x: 2, y: 1, op: op1))

        // How to use anonymous function ex.: 1
        XCTAssertTrue(executeOp(x: 1, y: 2,
                                op: { (x: Int, y: Int) -> Bool in x < y }))
        // ex.: 2
        XCTAssertTrue(executeOp(x: 1, y: 2,
                                op: { (x, y) -> Bool in x < y }))
        // ex.: 3
        XCTAssertTrue(executeOp(x: 1, y: 2, op: { x, y in x < y }))

        // ex.: 4
        XCTAssertTrue(executeOp(x: 1, y: 2, op: { $0 < $1 }))
        // Execute trail function
        XCTAssertEqual("Hello", exeuteOpWithoutParameter { "Hello" })

        // example define and invoke.
        let string:String = { "Hello" + "-2" }()
        
        XCTAssertEqual("Hello-2", string)
    }
}
