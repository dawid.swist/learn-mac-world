//
//  Ch02FunctionalClosure.swift
//  iOS-12-introducingTests
//
//  Created by Dawid Świst on 05/05/2020.
//

import XCTest

class Ch02FunctionalClosure: XCTestCase {
    func testDefineClosure() throws {
        let x = 10

        func f() -> Int {
            return x + 10
        }

        XCTAssertEqual(20, f())
    }

    func testClosueUpdatesVariable() {
        func makeIncrementer(forIncrement amount: Int) -> () -> Int {
            var runningTotal = 0
            func incrementer() -> Int {
                runningTotal += amount
                return runningTotal
            }
            return incrementer
        }

        let incrementByTen = makeIncrementer(forIncrement: 10)

        XCTAssertEqual(10, incrementByTen())
        XCTAssertEqual(20, incrementByTen())
        XCTAssertEqual(30, incrementByTen())
    }
    
    func testFunctionCurrying() {
        func multiply2(_ x: Int) -> (Int) -> Int {
            return { $0 * x }
        }

        XCTAssertEqual(20, multiply2(4)(5))
    }

}
