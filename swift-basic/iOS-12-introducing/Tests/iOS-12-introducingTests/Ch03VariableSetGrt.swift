//
//  Ch03VariableSetGrt.swift
//  iOS-12-introducingTests
//
//  Created by Dawid Świst on 12/05/2020.
//

import XCTest

class Ch03VariableSetGrt: XCTestCase {
    static let n0 = 1

    class A {
        var n1: Int = {
            if Ch03VariableSetGrt.n0 == 0 {
                return 1
            } else {
                return -1
            }
        }()
    }

    func testCheckCalculatedVariable() throws {
        let a = A()
        XCTAssertEqual(-1, a.n1)
    }

    func testShouldUseGetAndSet() {
        class A {
            var didN3Value = 0
            var willN3Value = 0
            
            private var _n3 = 0 {
                willSet {
                    willN3Value = newValue
                }
                didSet {
                    didN3Value = oldValue
                }
            }

            var n3: Int {
                get {
                    return _n3 * 10
                }
                set {
                    _n3 = newValue / 10
                }
            }
            
            var n3Double:Double {
                get {
                    return Double(_n3) // Convert from Int to Dobuble
                }
            }

        }

        let a = A()

        a.n3 = 20

        XCTAssertEqual(20, a.n3)
        XCTAssertEqual(0, a.didN3Value)
        XCTAssertEqual(2, a.willN3Value)
        XCTAssertEqual(2.0, a.n3Double)
    }
}
