import XCTest

import iOS_12_introducingTests

var tests = [XCTestCaseEntry]()
tests += iOS_12_introducingTests.allTests()
XCTMain(tests)
