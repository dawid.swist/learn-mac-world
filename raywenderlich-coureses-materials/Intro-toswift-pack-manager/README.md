# Files from raywenderlich - An Introduction to Swift Package Manager

Following locations contains file from my experiments about Swift Package Manager. 
Link to orginal test: https://www.raywenderlich.com/1993018-an-introduction-to-swift-package-manager

List of used command


```bash
swift package init --type=executable
swift package generate-xcodeproj
swift build
swift package update
swift run Website
swift test
swift run --configuration release
```